class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.text :title
      t.text :contentshort
      t.text :contentlong
      t.date :date
      t.timestamps null: false
    end
  end
end

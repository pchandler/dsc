class ArticlesController < ApplicationController
    # http://tutorials.jumpstartlab.com/projects/articlesger.html#i3:-tagging
    before_action :authenticate_user!, :except => [:show, :index, :archived, :category]
    def index
        @articles = Article.order("date DESC").all #Article.where('extract(year from date) = ?', 2015)
        # @articles = Article.order("date DESC").all.paginate(page: params[:page], :per_page => 5)
        @tags = Tag.all
        @articles_by_month = Article.order("date DESC").all.group_by { |article| article.date.strftime("%B %Y") }
        @latests = Article.order("date DESC").limit(3)
    end
       
    def archived
      @articles =  Article.order("date DESC").where(['extract(year from date) = ?', params[:month].split(' ').last]).where(['extract(month from date) = ?', Date::MONTHNAMES.index(params[:month].split(' ').first)])
      @tags = Tag.all
      @articles_by_month = Article.order("date DESC").all.group_by { |article| article.date.strftime("%B %Y") }
    end
    
    def category
      # http://stackoverflow.com/questions/18908608/rails-display-all-posts-with-current-category-id
      @tag = Tag.find_by_name(params[:tag])
      @articles = @tag.articles.order("date DESC").paginate(page: params[:page], :per_page => 5) 
      @tags = Tag.all
      @articles_by_month = Article.order("date DESC").all.group_by { |article| article.date.strftime("%B %Y") }
    end
    
    # http://commandercoriander.net/blog/2013/09/15/customizing-the-rails-index-action/
    # http://stackoverflow.com/questions/15060479/multiple-where-conditions-in-rails
    # http://stackoverflow.com/questions/15498723/how-do-i-convert-a-month-name-into-a-month-integer-in-ruby
   
      
    def archives_list
      @archive = Archive.order("date DESC").where('extract(year  from date) = ?', params[:year]).where('extract(month  from date) = ?', params[:month]).order("date DESC")
    end  
    def archives
    end
   
    def new
        @article = Article.new
       
    end
    
    def admin
      @articles = Article.all
    end
    
    def edit
      @article= Article.find(params[:id])
     
    end
    
   
    
    
    def create
         @article = Article.new(article_params)
         @article.save
        redirect_to @article
    end
    
    def show
        # @article = Article.find(params[:id])
        @article = Article.find(params[:id]) #@article = Article.where('extract(year from date) = ?', params[:id])
        @tags = Tag.all
        @articles_by_month = Article.all.group_by { |article| article.date.strftime("%B %Y") }
        @latests = Article.order("date DESC").limit(3)
    end
    
    def update
      @article = Article.find(params[:id])
      @article.update(article_params)

      redirect_to article_path(@article)
    end
    
    
    private
            def article_params
              params.require(:article).permit(:title, :contentshort, :contentlong, :date, :tag_list)
            end
end
